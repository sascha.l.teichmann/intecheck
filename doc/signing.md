# How to create signed server JSON database

## Create a master database

```
$ sudo  ./createindex  --ignore '^/(home|media|proc|run|snap|sys|tmp|var)' / | gzip -9 > database.json.gz
```

## Create a public/private key pair

Create a signing key (with password `test`):
```
$ openssl genrsa -passout pass:test -aes256 -out signing_priv.pem 2048
```


Export a public key:
```
$ openssl rsa -passin pass:test -in signing_priv.pem -outform PEM -pubout -out signing_pub.pem
```

Sign the database:

```
$ openssl dgst -passin pass:test -sha256 -sign signing_priv.pem  -out database.json.gz.sha256 database.json.gz
$ openssl base64 -in database.json.gz.sha256 -out database.json.gz.sha256.base64
```

Put `database.json.gz` and `database.json.gz.sha256.base64` on your web server.



To check the signature generation works:
```
$ openssl dgst -sha256 -verify signing_pub.pem -signature database.json.gz.sha256 database.json.gz
```

