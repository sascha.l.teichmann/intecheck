package main

import (
	"bytes"
	"compress/gzip"
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type index struct {
}

func loadPEM(fname string) (*pem.Block, error) {
	bytes, err := ioutil.ReadFile(fname)
	if err != nil {
		return nil, err
	}
	block, _ := pem.Decode(bytes)
	if block == nil {
		return nil, errors.New("not an PEM file")
	}
	return block, nil
}

func fetchFile(url string) ([]byte, error) {

	client := http.Client{}

	res, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("status code error: %d %s",
			res.StatusCode, res.Status)
	}

	return ioutil.ReadAll(res.Body)
}

func unmarshal(r io.Reader) (*index, error) {
	return nil, errors.New("Not implemented, yet!")
}

func deserializeIndex(r io.Reader, zipped bool) (*index, error) {
	if zipped {
		rin, err := gzip.NewReader(r)
		if err != nil {
			return nil, err
		}
		defer rin.Close()
	}

	return unmarshal(r)
}

func main() {
	var (
		key = flag.String("key", "signing_pub.pem", "Public key to verify database signature")
		url = flag.String("url", "http://localhost:8000/database.json.gz", "URL to fetch database from")
	)
	flag.Parse()

	log.Printf("info: pub %s\n", *key)
	log.Printf("info: url %s\n", *url)

	block, err := loadPEM(*key)
	if err != nil {
		log.Fatalf("error: Cannot load key: %v\n", err)
	}

	publicKey, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		log.Fatalf("error: Cannot extract public key: %v\n", err)
	}

	pubKey, ok := publicKey.(*rsa.PublicKey)
	if !ok {
		log.Fatalln("error: pub key is not RSA")
	}

	rawSignature, err := fetchFile(*url + ".sha256.base64")
	if err != nil {
		log.Fatalf("error: Cannot fetch signature from '%s.sha256.base64: %v'\n", *url, err)
	}

	signature := make([]byte, base64.StdEncoding.DecodedLen(len(rawSignature)))
	n, err := base64.StdEncoding.Decode(signature, rawSignature)
	if err != nil {
		log.Fatalf("error: Base64 decoding of signature failed: %v\n", err)
	}
	signature = signature[:n]

	idx, err := fetchFile(*url)
	if err != nil {
		log.Fatalf("error: Cannot fetch index from url '%s': %v\n", *url, err)
	}

	hash := sha256.Sum256(idx)

	if err := rsa.VerifyPKCS1v15(pubKey, crypto.SHA256, hash[:], signature); err != nil {
		log.Fatalf("error: Signature validation failed: %v\n", err)
	}

	log.Println("info: Signature validation succeeded")

	zipped := strings.HasSuffix(strings.ToLower(*url), ".gz")

	index, err := deserializeIndex(bytes.NewReader(idx), zipped)
	if err != nil {
		log.Fatalf("error: de-serializing index failed: %v\n", err)
	}
	_ = index
}
