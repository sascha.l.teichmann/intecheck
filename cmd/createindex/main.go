package main

import (
	"bufio"
	"crypto/sha256"
	"encoding/json"
	"flag"
	"fmt"
	"hash"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"sort"
	"strings"
	"sync"

	"golang.org/x/crypto/ripemd160"
)

type node struct {
	name     string
	checksum string
	children []*node
	info     os.FileInfo
}

type index struct {
	hash   func() hash.Hash
	ignore *regexp.Regexp
	roots  []*node
}

func readDirNames(path string) ([]string, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	names, err := f.Readdirnames(-1)
	f.Close()
	if err != nil {
		return nil, err
	}
	sort.Strings(names)
	return names, nil
}

type check struct {
	path string
	node *node
}

func (idx *index) checksumFile(path string) (string, error) {
	f, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer f.Close()

	var h hash.Hash
	if idx.hash != nil {
		h = idx.hash()
	} else {
		h = sha256.New()
	}
	if _, err := io.Copy(h, f); err != nil {
		return "", err
	}
	return string(h.Sum(nil)), nil
}

func (idx *index) checksum(wg *sync.WaitGroup, files <-chan check, errCh chan<- error) {

	defer wg.Done()

	for f := range files {
		ch, err := idx.checksumFile(f.path)
		if err != nil {
			errCh <- err
		} else {
			f.node.checksum = ch
		}
	}
}
func (idx *index) accept(path string) (os.FileInfo, error) {
	if idx.ignore != nil && idx.ignore.MatchString(path) {
		log.Printf("ignore: %s\n", path)
		return nil, nil
	}
	info, err := os.Lstat(path)
	// TODO: More filtering
	return info, err
}

func (idx *index) recurse(parent *node, path, name string, files chan<- check) error {

	fname := filepath.Join(path, name)

	info, err := idx.accept(fname)
	if err != nil || info == nil {
		return err
	}

	n := &node{
		name: name,
		info: info,
	}
	parent.children = append(parent.children, n)

	if !info.IsDir() {
		if info.Mode().IsRegular() {
			files <- check{
				path: fname,
				node: n,
			}
		}
		return nil
	}

	names, err := readDirNames(fname)
	if err != nil {
		return err
	}

	n.children = make([]*node, 0, len(names))

	for _, name := range names {
		if err := idx.recurse(n, fname, name, files); err != nil {
			return err
		}
	}

	return nil
}

func (idx *index) buildRoot(root string) (*node, error) {

	info, err := idx.accept(root)
	if info == nil || err != nil {
		return nil, err
	}

	var chanErr error

	errCh := make(chan error)
	errDone := make(chan struct{})

	go func() {
		defer close(errDone)
		for err := range errCh {
			if chanErr != nil {
				chanErr = err
			}
			log.Printf("warn: %v\n", err)
		}
	}()

	var wg sync.WaitGroup
	files := make(chan check)

	for n := runtime.NumCPU(); n > 0; n-- {
		wg.Add(1)
		go idx.checksum(&wg, files, errCh)
	}

	rootNode := &node{
		name: root,
		info: info,
	}

	if !info.IsDir() {
		if info.Mode().IsRegular() {
			files <- check{
				path: root,
				node: rootNode,
			}
		}
		close(files)
		wg.Wait()
		close(errCh)
		<-errDone
		return rootNode, nil
	}

	names, err := readDirNames(root)

	rootNode.children = make([]*node, 0, len(names))

	for _, name := range names {
		if err := idx.recurse(rootNode, root, name, files); err != nil {
			return nil, err
		}
	}

	close(files)
	wg.Wait()
	close(errCh)
	<-errDone

	return rootNode, nil
}

func (idx *index) buildRoots(paths []string) error {

	for _, path := range paths {
		root, err := idx.buildRoot(path)
		if err != nil {
			return err
		}
		if root != nil {
			idx.roots = append(idx.roots, root)
		}
	}

	return nil
}

func jsonString(s string) string {
	x, _ := json.Marshal(s)
	return string(x)
}

func (n *node) writeJSON(w io.Writer) error {
	fmt.Fprintf(w, `{"n":%s,"m":%o`, jsonString(n.name), n.info.Mode())
	if n.info.Mode().IsRegular() {
		if n.checksum != "" {
			fmt.Fprintf(w, `,"h":"%x"`, n.checksum)
		}
		fmt.Fprintf(w, `,"s":%d`, n.info.Size())
	}
	if len(n.children) > 0 {
		fmt.Fprint(w, `,"f":[`)
		for i, child := range n.children {
			if i > 0 {
				fmt.Fprint(w, ",")
			}
			if err := child.writeJSON(w); err != nil {
				return err
			}
		}
		fmt.Fprint(w, `]`)
	}
	_, err := fmt.Fprintf(w, "}")
	return err
}

func (idx *index) writeJSON(w io.Writer) error {
	out := bufio.NewWriter(w)
	fmt.Fprint(out, `{"roots":[`)
	for i, root := range idx.roots {
		if i > 0 {
			fmt.Fprint(out, ",")
		}
		if err := root.writeJSON(out); err != nil {
			return err
		}
	}
	fmt.Fprintln(out, `]}`)
	return out.Flush()
}

func whichHash(name string) func() hash.Hash {
	switch strings.ToLower(name) {
	case "sha256":
		return sha256.New
	case "ripemd160":
		return ripemd160.New
	default:
		return nil
	}
}

func main() {

	var (
		ignore = flag.String("ignore", "", "regexp of files to ignore")
		hash   = flag.String("hash", "", "hash to use (default SHA256)")
	)

	flag.Parse()

	var paths []string

	if flag.NArg() == 0 {
		paths = []string{"/"}
	} else {
		paths = flag.Args()
	}

	idx := index{
		hash: whichHash(*hash),
	}

	if *ignore != "" {
		re, err := regexp.Compile(*ignore)
		if err != nil {
			log.Fatalf("error: %v\n", err)
		}
		idx.ignore = re
	}

	if err := idx.buildRoots(paths); err != nil {
		log.Fatalf("error: %v\n", err)
	}

	if err := idx.writeJSON(os.Stdout); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
